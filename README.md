### rad deployments

Note: All deployments are done in a single cluster, the _test_ cluster.

In a production application deployments should be deployed to at least to environments, `staging` and `production`.